#!/bin/bash

# Pushes images to registry for tags in blender_urls.txt

while read a b; do echo;echo "Pushing image registry.gitlab.com/soerenmetje/docker-blender:${a}"; docker push registry.gitlab.com/soerenmetje/docker-blender:"${a}"; done < blender_urls.txt
