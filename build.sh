#!/bin/bash

# Builds images for tags and urls in blender_urls.txt

while read a b; do docker build --build-arg BLENDER_URL="${b}" -t registry.gitlab.com/soerenmetje/docker-blender:"${a}" .; done < blender_urls.txt

# test
while read a b; do echo;echo; echo "TEST expect Blender ${a}"; docker run --rm registry.gitlab.com/soerenmetje/docker-blender:"${a}" -v; done < blender_urls.txt
